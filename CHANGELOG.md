# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.7.0] - 2019-08-05
### Fixed
- Inconsistencies in Address and definitions of Rx/Tx Registers

